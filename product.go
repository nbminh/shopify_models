package models

import "gorm.io/gorm"

type Product struct {
	gorm.Model
	ProductID int64
	Name      string
	Price     string
	Sku       string
	Type      string
	Status    string
}
