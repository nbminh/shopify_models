module gitlab.com/nbminh/shopify_models

go 1.17

require gorm.io/gorm v1.24.3

require (
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/jinzhu/now v1.1.4 // indirect
)
